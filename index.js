const initTribute = (element) => {
  const values = [
    {key: 'Phil Heartman', value: 'pheartman'},
    {key: 'Gordon Ramsey', value: 'gramsey'}
  ];
  new Tribute({
    values: values
  }).attach(element);
};

const getSubdoc = () => document
  .querySelector('iframe')
  .contentDocument;

const init = () => {
  initTribute(getSubdoc().querySelector('.editor'));
};

document
  .querySelector('iframe')
  .addEventListener('load', init);
